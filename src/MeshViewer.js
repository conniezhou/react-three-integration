import React, { Component } from 'react';
import ViewerUI from './ViewerUI';
import MeshCollection from './MeshCollection';
import ReactDOM from 'react-dom';

export default class MeshViewer extends React.Component{
    // //constructor(viewer_id, metadata, data_path, title = null)
    //     constructor(props) {
    //     // super(viewer_id, metadata, data_path, title);
    //     this.data_path = this.props.data_path;
    //     let container = document.getElementById(viewer_id);
    //     this.frame_ix = 0;
    //     this.ui = new ViewerUI(this.props.metadata, container, this.props.title);
    //     // this.ui.gl.manager.onLoad = () => this.update_visible();
    //     this.load_meshes(this.props.metadata);
    //     this.register_ui_cbks(this.props.metadata);
    //     this.update_viewport(true);
    //     this.animate();
    // }

    // copy_view_from(other) {
    //     this.ui.gl.camera.copy(other.ui.gl.camera);
    // }

    // on_animate() {}

    // load_meshes(json) {
    //     this.layers = {};
    //     let mesh_md = json.isosurfaces;
    //     for (let surf_name in mesh_md) {
    //         let mesh_collection = new MeshCollection(mesh_md[surf_name]);
    //         let obj_id = mesh_collection.obj_id;
    //         for (let j = 0; j < mesh_collection.files.length; j++) {
    //             let iso_files = mesh_collection.files[j];
    //             // the order of mesh loads is not guarenteed. must index explicitly into array
    //             // instead of pushing
    //             mesh_collection.meshes[j] = new Array(iso_files.length);

    //             for (let i = 0; i < iso_files.length; i++) {
    //                 let fname = `${this.data_path}/${iso_files[i]}`;
    //                 this.ui.gl.loader.load(
    //                     fname,
    //                     gltf => {
    //                         let m = gltf.scene.getObjectByName(obj_id);
    //                         mesh_collection.meshes[j][i] = m;
    //                         m.visible = false;
    //                         this.ui.gl.scene.add(gltf.scene);
    //                     },
    //                     undefined,
    //                     e => console.error(e)
    //                 );
    //             }
    //         }
    //         this.layers[surf_name] = mesh_collection;
    //     }
    //     return json;
    // }

    // register_ui_cbks(json) {
    //     window.addEventListener('resize', () => this.update_viewport());

    //     //this.ui.viewport.onmouseout = () => (this.active_view = false);
    //     // this.ui.viewport.onmouseover = () => (this.active_view = true);

    //     // mesh layers
    //     for (let key in this.layers) {
    //         let ctrl = this.ui.layer_controls[key];

    //         // slide
    //         if (ctrl.slider) {
    //             let slide_oldcbk = ctrl.slider.oninput;
    //             ctrl.slider.oninput = () => {
    //                 slide_oldcbk();
    //                 this.layers[key].set_iso(ctrl.slider.getIndex());
    //             };
    //         }

    //         // button
    //         let click_oldcbk = ctrl.button.onclick;
    //         ctrl.button.onclick = () => {
    //             click_oldcbk();
    //             this.layers[key].set_visible(!ctrl.button.click_state);
    //             this.update_viewport();
    //         };
    //     }

    //     return json;
    // }

    // set_frame(ix) {
    //     this.frame_ix = ix;
    //     for (let key in this.layers) {
    //         this.layers[key].set_frame(ix);
    //     }
    //     this.update_viewport();
    // }

    // update_visible() {
    //     for (let key in this.layers) {
    //         this.layers[key].update();
    //     }
    //     this.update_viewport();
    // }

    // animate() {
    //     window.requestAnimationFrame(() => {
    //         this.animate();
    //     });
    //     this.on_animate();
    //     this.update_viewport();
    // }

    // update_viewport(force = false) {
    //     // let container_dim = this.ui.viewport.getBoundingClientRect();
    //     // TODO
    //     const canvas = this.ui.gl.renderer.domElement;
    //     if (force || canvas.width !== canvas.clientWidth || canvas.height !== canvas.clientHeight) {
    //         this.ui.gl.camera.aspect = container_dim.width / container_dim.height;
    //         this.ui.gl.camera.updateProjectionMatrix();
    //         this.ui.gl.renderer.setSize(container_dim.width, container_dim.height, false);
    //         this.ui.gl.controls.update();
    //     }

    //     this.ui.gl.renderer.render(this.ui.gl.scene, this.ui.gl.camera);
    // }

    // render(){
    //     return(
    //         <div>
    //             <h1>true</h1>
    //             </div>
    //     );
    // }
}
