import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import './index.css';
import PlayControl from './PlayControl';
import ViewerUI from './ViewerUI';
import MeshCollection from './MeshCollection'
import MeshViewer from './MeshViewer'
import SimpleViewer from './SingleViewer';

ReactDOM.render(
  <SimpleViewer />,
  document.getElementById('srfbaview-viewer')
);
