import React, { Component } from 'react';
import { Player, ControlBar } from 'video-react';
import { Button } from 'reactstrap';

const sources = {
  sintelTrailer: 'http://media.w3.org/2010/05/sintel/trailer.mp4',
  bunnyTrailer: 'http://media.w3.org/2010/05/bunny/trailer.mp4',
  bunnyMovie: 'http://media.w3.org/2010/05/bunny/movie.mp4',
  test: 'http://media.w3.org/2010/05/video/movie_300.webm'
};

export default class PlayerControlExample extends Component {
  constructor(props, context) {
    super(props, context);

    this.state = {
      source: sources.bunnyMovie
    };

    this.play = this.play.bind(this);
    this.pause = this.pause.bind(this);
    this.load = this.load.bind(this);
    this.changeCurrentTime = this.changeCurrentTime.bind(this);
    this.seek = this.seek.bind(this);
  }

  componentDidMount() {
    // subscribe state change
    this.player.subscribeToStateChange(this.handleStateChange.bind(this));
  }

  set_frame(ix) {
    this.callbacks.forEach(fn => fn(ix));
    }

  handleStateChange(state) {
    // copy player state to this component's state
    this.setState({
      player: state
    });
  }

  play() {
    this.player.play();
  }

  pause() {
    this.player.pause();
  }

  load() {
    this.player.load();
  }

  changeCurrentTime(seconds) {
    return () => {
      const { player } = this.player.getState();
      this.player.seek(player.currentTime + seconds);
    };
  }

  seek(seconds) {
    return () => {
      this.player.seek(seconds);
    };
  }

  render() {
    return (
      <div>
        <Player
          ref={player => {
            this.player = player;
          }}
          autoPlay
        >
          <source src={this.state.source} />
          <ControlBar autoHide={false} />
        </Player>
        <div className="py-3">
          <Button onClick={this.play} className="mr-3">
            play()
          </Button>
          <Button onClick={this.pause} className="mr-3">
            pause()
          </Button>
          <Button onClick={this.load} className="mr-3">
            load()
          </Button>
        </div>
      </div>
    );
  }
}

// import React, { Component } from 'react';
// import { Button } from 'reactstrap';
// // import { PrismCode } from 'react-prism';
// import { Player, ControlBar } from 'video-react';

// const sources = [];

// class PlayControl extends React.Component{
//     constructor(props, context){
//         super(props, context);

//         this.state = {
//         source: sources.bunnyMovie
//     };

//     this.play = this.play.bind(this);


// }
//     render() {
//         return (
//             <div>
//             <Player
//           ref={player => {
//             this.player = player;
//           }}
//           autoPlay
//         >
//           <source src={this.state.source} />
//           <ControlBar autoHide={false} />
//         </Player>
//           <div className="play-ctrl">
//             {/* row */}
//             row
//             {/* col */}
//             col
//             {/* button */}
//             <Button onClick={this.pause} className="play">
//                 </Button>
//           </div>
//           </div>
//         );
//       }
// }

// export default PlayControl;
// class PlayControl extends React.Component{
//     register_callback(fn) {
//         this.callbacks.push(fn);
//     }

//     set_frame(ix) {
//         this.callbacks.forEach(fn => fn(ix));
//     }

//     constructor(props) {
//             super(props);
//             this.playback_period = 5000; // trajectory playback on a 5 sec loop
//             this.callbacks = [];
//             this.play_icons = ['stop', 'play'];
//             let play_ctrl = build_control_row(container, '', json.time, this.play_icons, x =>
//                 sprintf('Time=%.3f %s', x, json.time_axis_label));
//             play_ctrl.row_div.className = 'play-ctrl';
//             this.controls = play_ctrl;
        
//             // this.build_control_row = this.build_control_row.bind(this);
//     }

//     build_control_row(control_pane, name, values, symbols, fmt_fn) {
//         let dom_row = {};
    
//         // row
//         let row_div = document.cr1eateElement('div');
//         control_pane.appendChild(row_div);
//         dom_row.row_div = row_div;
    
//         // label
//         let cell_label = document.createElement('div');
//         row_div.appendChild(cell_label);
//         cell_label.innerHTML = name;
//         dom_row.label = cell_label;
    
//         // button
//         let cell_button = document.createElement('div');
//         row_div.appendChild(cell_button);
//         let button = document.createElement('button');
//         button.className = symbols[1];
//         button.click_state = false;
//         cell_button.appendChild(button);
//         button.onclick = () => {
//             button.click_state = !button.click_state;
//             var ix = button.click_state ? 0 : 1;
//             button.className = symbols[ix];
//         };
//         dom_row.button = button;
    
//         // status
//         let cell_status = document.createElement('div');
//         dom_row.status = cell_status;
    
//         // slider
//         let cell_slider = document.createElement('div');
//         row_div.appendChild(cell_slider);
    
//         if (values.length > 1) {
//             let slider = document.createElement('input');
//             slider.type = 'range';
//             slider.max = values.length - 1;
//             slider.min = 0;
//             slider.value = 0;
//             slider.step = 1;
//             cell_status.innerHTML = fmt_fn(values[0]);
//             slider.getIndex = () => parseInt(slider.value);
//             slider.setIndex = i => {
//                 slider.value = parseInt(i);
//                 cell_status.innerHTML = fmt_fn(values[slider.getIndex()]);
//             };
//             slider.oninput = () => (cell_status.innerHTML = fmt_fn(values[slider.getIndex()]));
    
//             cell_slider.appendChild(slider);
//             dom_row.slider = slider;
//         } else {
//             dom_row.slider = null;
//         }
    
//         row_div.appendChild(cell_status);
    
//         return dom_row;
//     }
    

//         // play control slide callback
//         slide_cbk = play_ctrl.slider.oninput;
//         this.play_ctrl.slider.oninput = () => {
//             slide_cbk();
//             this.set_frame(play_ctrl.slider.getIndex());
//         };

//         // play control button callback
//         let click_cbk = play_ctrl.button.onclick;
//         play_ctrl.button.onclick = () => {
//             click_cbk();
//             if (play_ctrl.button.click_state) {
//                 let delay = this.playback_period / json.time.length;
//                 this.playback_start_frame = play_ctrl.slider.getIndex();
//                 this.playback_timer = setInterval(() => {
//                     let cur = play_ctrl.slider.getIndex();
//                     let next = (cur + 1) % json.time.length;
//                     play_ctrl.slider.setIndex(next);
//                     this.set_frame(next);
//                 }, delay);
//             } else {
//                 clearInterval(this.playback_timer);
//                 play_ctrl.slider.value = this.playback_start_frame;
//                 play_ctrl.slider.setIndex(this.playback_start_frame);
//                 this.set_frame(this.playback_start_frame);
//             }
//         };
//     }
// }

// export default PlayControl;
