import React, { Component } from "react";
import ReactDOM from "react-dom";
import THREE from "./three";

const style = {
  height: 250 // we can control scene size by setting container dimensions
};

export default class ViewerUI extends Component {
  constructor(props) {
    super(props);
    this.build_single_viewframe(this.props.json);
    this.sceneSetup = this.sceneSetup.bind(this);
  } 

  componentDidMount() {
    this.sceneSetup();    
  }

  build_single_viewframe(json){

    this.container = this.props.container;
    this.viewport = document.getElementsByClassName('viewport');
    // title
    if(this.props.title!=null){
      document.title = this.props.title;
    }else{
      document.title = "ViewerUI Temp Title";
    }    
  }
  
  sceneSetup(){

    this.layer_icons = ['on', 'off'];
    this.container = this.props.container;
    this.title = this.props.title;

    const width = this.viewport.clientWidth;
    const height = this.viewport.clientHeight;
    
    const gl = {};
    this.gl = gl;
    this.gl.scene = new THREE.Scene();
    this.gl.camera = new THREE.PerspectiveCamera(
      45,
      window.innerWidth / window.innerHeight,
      0.25,
      20
    );
    this.gl.controls = new THREE.OrbitControls(this.gl.camera, this.viewport);
    this.gl.controls.enableDamping = true;
    this.gl.controls.target.set(0, 0, 0);
    this.gl.controls.update();

    this.gl.light = new THREE.HemisphereLight(0xaaaaaa, 0x444444);
    this.gl.light.position.set(0, 1, 0); 
    this.gl.scene.add(this.gl.light);

    this.gl.manager = new THREE.LoadingManager();

    THREE.DRACOLoader.setDecoderPath('/static/');
    this.gl.loader = new THREE.GLTFLoader(this.gl.manager)
            .setPath('./')
            .setDRACOLoader(new THREE.DRACOLoader());

    this.gl.renderer = new THREE.WebGLRenderer({ alpha: true, antialias: true });

    this.gl.renderer.setClearColor(0xffffff, 0);
    this.gl.renderer.setPixelRatio(window.devicePixelRatio);
    this.gl.renderer.setSize(width, height, false);
    this.gl.renderer.gammaOutput = true;
    this.viewport.appendChild(this.gl.renderer.domElement);

  }

  render() {
    return (
    <div style={style} ref={ref => (this.viewport = ref)}>
      <div>
        <h1>work?</h1>
        </div>
      </div>
      );
  }
}