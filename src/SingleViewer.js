import React, { Component } from 'react';
import MeshViewer from './MeshViewer';
import PlayControl from './PlayControl';

import ReactDOM from 'react-dom';

export default class SimpleViewer extends React.Component{
    // two parameters: viewer_id, data_path
       constructor(props){
        super(props);
        

        this.metadata_st = "test";
        document.title = props.viewer_id;
        let dom_play = document.createElement('div');
        let play_id = this.props.viewer_id + '-play-control';
        dom_play.classList.add('ctrl-table');
        dom_play.id = play_id;

        let dom_view = document.createElement('div');
        this.mv_id = this.props.viewer_id + '-viewer';
        dom_view.id = this.mv_id;
        dom_view.classList.add('simple-view');

        // let container = document.getElementById(this.props.viewer_id);
        // HARD CODED RIGHT NOW
        let container = document.getElementById("srfbaview");
        container.appendChild(dom_play);
        container.appendChild(dom_view);

    }

    componentDidMount() {
        // fetch(`${this.props.data_path}/meta.json`)
        // HARD CODED RIGHT NOW
        const data_path = "/model"
        fetch(`${data_path}/meta.json`, {
            headers : { 
              'Content-Type': 'application/json',
              'Accept': 'application/json'
             }
      
          })
            .then(req => req.json())
            .then(metadata => {
                this.viewer = new MeshViewer(this.mv_id, metadata, this.props.data_path);
                this.metadata_st = metadata.creation_time;
                console.log(this.metadata_st);
                return metadata;
            })
            .then(metadata => {
                this.play = new PlayControl(metadata, this.dom_play);
                // TODO
                // this.play.register_callback(ix => this.viewer.set_frame(ix));
                return metadata;
            });
      }



    render() {
        return (
        <div>
                <h1>
                    the creation time from metadata is {this.metadata_st}
                </h1>
          </div>
          );
      }
}