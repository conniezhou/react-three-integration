import * as THREE from 'three';


window.THREE = THREE; // THREE.DRACOLoader expects THREE to be a global object
require('three/examples/js/loaders/GLTFLoader');
require('three/examples/js/loaders/DRACOLoader');
require('three/examples/js/controls/OrbitControls');

export default window.THREE;